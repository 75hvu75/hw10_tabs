'use strict';

const tabsName = document.querySelector('.tabs');
const tabsContent = document.querySelector('.tabs-content');

tabsName.addEventListener('click', e => {
    if (e.target !== tabsName) {
        [...tabsName.children].forEach(item => {
            item.classList.remove('active');
        });

        [...tabsContent.children].forEach(tab => {
            if (tab.dataset.tabs === e.target.dataset.tabs) {
                tab.classList.add('active');
            } else {
                tab.classList.remove('active');
            }
        });

        e.target.classList.toggle('active');
    }
});
